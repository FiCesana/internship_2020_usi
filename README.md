## Summer Internship 2020 at Università della Svizzera italiana

This repository is hosting my progress in terms of code and knowledge as an intern for the project "Python front-end for Utopia, 
a C++ library for parallel scientific computing and xSDK compilance"  organized by Prof. Dr. Krause, Dr. Patrick Zullian, 
and Dr. Alena Kopanicakova. Given this title, possible side or paralle projects, as well as the devolpment of an interactive demo 
should or may occur during the summer. The project is linked to the Institute of Computational Science.