#include <iostream>
#include <string>
using namespace std;

int main(int argc, const char * argv[]) {
    
    if (argc == 1 or argc > 2) {
        string name;
        cout << "Please enter a name." << endl;
        cin >> name;
        cout << "Hello, " << name << endl;
    }
    else {
        cout << "Hello, " << argv[1] << endl;
    }
    
    return 0;
}
